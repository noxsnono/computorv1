/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_expose2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/03 13:05:34 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/04/03 13:05:35 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

static void	cal_expose2_4(t_data *d, float delta)
{
	float	result1;
	float	result2;

	result1 = ((d->expos_left[1]->mult * -1) - ft_sqrt(delta))
			/ (2 * d->expos_left[2]->mult);
	result2 = ((d->expos_left[1]->mult * -1) + ft_sqrt(delta))
			/ (2 * d->expos_left[2]->mult);
	ft_putstr("solution 1:");
	ft_putchar('\t');
	ft_putfloat(result1);
	ft_putchar('\n');
	ft_putstr("solution 2:");
	ft_putchar('\t');
	ft_putfloat(result2);
	ft_putchar('\n');
}

static void	cal_expose2_3(t_data *d, float delta)
{
	float	result1;
	float	result2;

	result1 = (d->expos_left[1]->mult * -1);
	result2 = 2 * d->expos_left[2]->mult;
	ft_putstr("solution 1:");
	ft_putchar('\t');
	ft_putfloat(result1);
	ft_putstr(" - i * sqrt(");
	ft_putfloat(delta);
	ft_putstr(") / ");
	ft_putfloat(result2);
	ft_putchar('\n');
	result1 = (d->expos_left[1]->mult * -1);
	result2 = 2 * d->expos_left[2]->mult;
	ft_putstr("solution 2:");
	ft_putchar('\t');
	ft_putfloat(result1);
	ft_putstr(" + i * sqrt(");
	ft_putfloat(delta);
	ft_putstr(") / ");
	ft_putfloat(result2);
	ft_putchar('\n');
}

static void	cal_expose2_2(t_data *d)
{
	float	result1;

	result1 = (d->expos_left[1]->mult * -1) / (2 * d->expos_left[2]->mult);
	ft_putstr("solution is: ");
	ft_putfloat(result1);
	ft_putchar('\n');
}

int			cal_expose2(t_data *d)
{
	float	delta;
	float	r_4ac;
	float	r_bxb;

	delta = 0;
	r_4ac = 0;
	r_bxb = 0;
	r_bxb = d->expos_left[1]->mult * d->expos_left[1]->mult;
	r_4ac = 4 * d->expos_left[2]->mult * (d->expos_left[0]->mult
				+ d->left_toadd);
	delta = r_bxb - r_4ac;
	if (delta == 0)
		cal_expose2_2(d);
	else if (delta < 0)
		cal_expose2_3(d, delta);
	else if (delta > 0)
		cal_expose2_4(d, delta);
	return (0);
}
