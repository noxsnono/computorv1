/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_expos.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 16:16:35 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/03/21 16:16:38 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int	count_expos(t_data *d, char **av)
{
	int		i;
	char	*tmp;

	w_log("Enter to count_expos");
	d->max_expos = 0;
	i = 0;
	while (av[1][i] != '\0')
	{
		if (av[1][i] == '^')
		{
			if (av[1][i + 1] == '\0' || ft_isdigit(av[1][i + 1]) != 1)
				return (error("count_expos:: bad character after '^'"));
			if (d->max_expos < ft_atoi(&av[1][i + 1]))
			{
				d->max_expos = ft_atoi(&av[1][i + 1]);
				i += 2;
			}
		}
		if (av[1][i] != '\0')
			i++;
	}
	tmp = ft_itoa(d->max_expos);
	ft_memdel((void **)&tmp);
	return (0);
}
