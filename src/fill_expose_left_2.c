/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_expose_left_2.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 11:41:42 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/04/04 11:41:43 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int		fill_expose_left_1(t_data *d)
{
	if (ft_isdigit(d->str[d->p_i]) == 1)
	{
		d->number = 0.0f;
		d->number = ft_atof(&d->str[d->p_i]);
		while (d->str[d->p_i] != '\0' && (ft_isdigit(d->str[d->p_i]) == 1
			|| d->str[d->p_i] == ' ' || d->str[d->p_i] == '.'))
			d->p_i++;
		if (d->left == 1 && d->str[d->p_i] != '\0' && d->str[d->p_i] == '*')
		{
			if (fill_expose_left_3(d) < 0)
				return (-1);
		}
		else if (d->left == 0 && d->str[d->p_i] != '\0'
			&& d->str[d->p_i] == '*')
		{
			if (fill_expose_left_2(d) < 0)
				return (-1);
		}
		else
			fill_expose_left_4(d);
	}
	fill_expose_left_5(d);
	return (0);
}

void	fill_expose_left_5(t_data *d)
{
	if (d->str[d->p_i] == '+' || d->str[d->p_i] == '=')
		d->negative = 0;
	else if (d->str[d->p_i] == '-')
		d->negative = 1;
}

int		fill_expose_left_2(t_data *d)
{
	while (d->str[d->p_i] != '\0' && (d->str[d->p_i] == ' '
		|| d->str[d->p_i] == '*'))
		d->p_i++;
	if (d->str[d->p_i] == 'X' && d->str[d->p_i + 1] == '^'
		&& ft_isdigit(d->str[d->p_i + 2]) == 1)
	{
		d->p_tmp = ft_atoi(&d->str[d->p_i + 2]);
		if (d->negative == 0)
			d->expos_left[d->p_tmp]->mult -= d->number;
		else
			d->expos_left[d->p_tmp]->mult += d->number;
		d->p_i += 2;
		while (d->str[d->p_i] != '\0' && d->str[d->p_i] == ' ')
			d->p_i++;
	}
	else
		return (error("fill_expose_left:: invalid char after 'X^'"));
	return (0);
}

int		fill_expose_left_3(t_data *d)
{
	while (d->str[d->p_i] != '\0' && (d->str[d->p_i] == ' '
		|| d->str[d->p_i] == '*'))
		d->p_i++;
	if (d->str[d->p_i] == 'X' && d->str[d->p_i + 1] == '^'
		&& ft_isdigit(d->str[d->p_i + 2]) == 1)
	{
		d->p_tmp = ft_atoi(&d->str[d->p_i + 2]);
		if (d->negative == 0)
			d->expos_left[d->p_tmp]->mult += d->number;
		else
			d->expos_left[d->p_tmp]->mult -= d->number;
		d->p_i += 2;
		while (d->str[d->p_i] != '\0' && d->str[d->p_i] == ' ')
			d->p_i++;
	}
	else
		return (error("fill_expose_left:: invalid char after 'X^'"));
	return (0);
}

void	fill_expose_left_4(t_data *d)
{
	if (d->left == 1)
	{
		if (d->negative == 0)
			d->expos_left[0]->mult += d->number;
		else
			d->expos_left[0]->mult -= d->number;
	}
	else
	{
		if (d->negative == 0)
			d->expos_left[0]->mult -= d->number;
		else
			d->expos_left[0]->mult += d->number;
	}
	while (d->str[d->p_i] != '\0' && (ft_isdigit(d->str[d->p_i]) == 1
		|| d->str[d->p_i] == ' ' || d->str[d->p_i] == '.'))
		d->p_i++;
}
