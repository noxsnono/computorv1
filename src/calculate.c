/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/03 10:44:27 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/04/03 10:44:28 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int	calculate(t_data *d)
{
	w_log("Enter to calculate");
	if (d->refac_max_expos == 1
		|| (d->refac_max_expos == 2 && d->expos_left[2]->mult == 0))
		cal_expose1(d);
	else if (d->refac_max_expos == 2)
		cal_expose2(d);
	else
		return (2);
	return (0);
}
