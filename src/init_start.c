/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_start.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 16:16:59 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/03/21 16:17:02 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int	init_start(t_data **d, int ac, char **av)
{
	int i;

	i = 0;
	(void)ac;
	if (ac < 2)
		return (error("init_start:: Require 2 args minimum"));
	while (av[1][i] != '\0')
	{
		if (av[1][i] != 'X' && av[1][i] != '^' && av[1][i] != ' '
			&& av[1][i] != '-' && av[1][i] != '+' && av[1][i] != '*'
			&& av[1][i] != '/' && av[1][i] != '.' && av[1][i] != '='
			&& ft_isdigit(av[1][i]) == 0)
			return (error("init_start:: Bad character on paramaters"));
		i++;
	}
	*d = get_data();
	if (NULL == d)
		return (error("init_start:: t_data init is NULL, malloc failed"));
	if (open_log(*d) < 0)
		return (error("init_start:: Open_log error"));
	return (0);
}
