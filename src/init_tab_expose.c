/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_tab_expose.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 16:16:35 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/03/21 16:16:38 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int	init_tab_expose(t_data *d)
{
	int	i;

	w_log("Enter to init_tab_expose");
	i = 0;
	d->expos_left = NULL;
	d->expos_left = (t_expos **)malloc(sizeof(t_expos *) * (d->max_expos + 1));
	if (d->expos_left == NULL)
		return (error("init_tab_expose:: expos_left Malloc error"));
	while (i < d->max_expos + 1)
	{
		d->expos_left[i] = (t_expos *)malloc(sizeof(t_expos));
		if (d->expos_left[i] == NULL)
			return (error("init_tab_expose:: expos_left Malloc error"));
		d->expos_left[i]->expos = i;
		d->expos_left[i]->mult = 0.0f;
		i++;
	}
	return (0);
}
