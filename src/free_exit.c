/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_exit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 16:16:35 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/03/21 16:16:38 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

void	free_exit(t_data *d)
{
	int	i;

	if (d != NULL)
	{
		if (d->expos_left != NULL)
		{
			i = 0;
			while (i < d->max_expos)
			{
				if (d->expos_left[i] != NULL)
					ft_memdel((void **)&d->expos_left[i]);
				i++;
			}
			if (d->expos_left != NULL)
				ft_memdel((void **)&d->expos_left);
		}
		ft_memdel((void **)&d);
	}
}
