/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_can_do_solution.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/02 18:42:14 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/04/02 18:42:15 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int	check_can_do_solution(t_data *d)
{
	w_log("Enter to check_can_do_solution");
	if (d->refac_max_expos == 0)
	{
		if (d->expos_left[0]->mult == 0)
			return (error("Tous les nombres reels sont solutions"));
		if (d->expos_left[0]->mult != 0)
			return (error("Equation Impossible"));
	}
	else if (d->refac_max_expos > 2)
		return (error("Ne peut pas resoudre un equation de degre 3 ou plus"));
	return (0);
}
