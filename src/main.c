/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 16:16:35 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/03/21 16:16:38 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int	main(int ac, char **av)
{
	t_data	*d;

	if (init_start(&d, ac, av) < 0)
		return (error("main:: Initialization error"));
	if (count_expos(d, av) < 0 || init_tab_expose(d) < 0
		|| fill_expose_left(d, av) < 0)
		return (error("main:: Parse error"));
	print_expose_left(d);
	check_can_do_solution(d);
	calculate(d);
	free_exit(d);
	return (0);
}
