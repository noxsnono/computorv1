/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_expose_left.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/02 17:53:18 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/04/02 17:53:18 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

static void	print_expose_left_2(t_data *d, int *i, int *before)
{
	if (d->expos_left[(*i)]->mult != 0.0f)
	{
		w_log("print_expose_left debugg 0");
		if (d->expos_left[(*i)]->mult >= 0.0f && (*before) != 0)
		{
			w_log("print_expose_left debugg 1");
			ft_putchar('+');
			ft_putchar(' ');
		}
		ft_putfloat(d->expos_left[(*i)]->mult);
		ft_putstr(" * X^");
		ft_putnbr(d->expos_left[(*i)]->expos);
		ft_putchar(' ');
		if (d->refac_max_expos < (*i))
			d->refac_max_expos = (*i);
		(*before)++;
	}
}

int			print_expose_left(t_data *d)
{
	int	i;
	int	before;

	w_log("Enter to print_expose_left");
	i = 0;
	before = 0;
	d->refac_max_expos = 0;
	ft_putstr("Reduced form:");
	ft_putchar('\t');
	while (i < d->max_expos + 1)
	{
		if (d->expos_left[i]->mult != 0.0f)
			print_expose_left_2(d, &i, &before);
		i++;
	}
	ft_putstr("= 0\n");
	ft_putstr("Polynomial degree: ");
	ft_putnbr(d->max_expos);
	ft_putchar('\n');
	return (0);
}
