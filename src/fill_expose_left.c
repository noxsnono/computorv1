/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_expose_left.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 16:16:35 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/03/21 16:16:38 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int		fill_expose_left(t_data *d, char **av)
{
	w_log("Enter to fill_expose_left");
	d->p_i = 0;
	d->p_tmp = 0;
	d->left = 1;
	d->negative = 0;
	d->str = av[1];
	d->p_len = ft_strlen(d->str);
	d->left_toadd = 0;
	while (d->str[d->p_i] != '\0')
	{
		if (fill_expose_left_1(d) < 0)
			return (-1);
		if (d->p_i < d->p_len && d->str[d->p_i] == '=')
			d->left = 0;
		if (d->p_i < d->p_len && d->str[d->p_i] != '\0')
			d->p_i++;
	}
	if (d->left == 1)
		return (error("fill_expose_left:: need sign '=' on equation"));
	return (0);
}
