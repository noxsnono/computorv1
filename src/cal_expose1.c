/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cal_expose1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/03 10:47:54 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/04/03 10:47:55 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/projet.h"

int	cal_expose1(t_data *d)
{
	float	left;
	float	right;
	float	result;

	w_log("enter to cal_expose1");
	left = 0;
	right = 0;
	left += d->expos_left[0]->mult;
	left += d->left_toadd;
	right = left * (-1);
	result = right / d->expos_left[1]->mult;
	ft_putstr("The solution is: ");
	ft_putfloat(result);
	ft_putchar('\n');
	return (0);
}
