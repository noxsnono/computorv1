/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projet.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 16:16:35 by jmoiroux          #+#    #+#             */
/*   Updated: 2015/03/21 16:16:38 by jmoiroux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROJET_H
# define PROJET_H

# include "../libft/includes/libft.h"

# define LOG_PATH	"logs/debugg.log"
# define DEBUGG 	0

typedef struct			s_float
{
	float				result;
	float				multiple;
	int					i;
	int					pts;
	int					negative;
}						t_float;

typedef struct			s_expos
{
	int					expos;
	float				mult;
}						t_expos;

typedef struct			s_data
{
	int					fd_log;
	int					i_args;
	float				*value;
	int					max_expos;
	int					refac_max_expos;
	int					negative;
	char				*str;
	int					p_i;
	int					left;
	float				number;
	int					p_len;
	int					p_tmp;

	t_expos				**expos_left;
	float				left_toadd;

	unsigned short int	op_log;
}						t_data;

int						count_expos(t_data *d, char **av);
int						init_tab_expose(t_data *d);
int						fill_expose_left(t_data *d, char **av);
void					free_exit(t_data *d);
int						print_expose_left(t_data *d);
int						calculate(t_data *d);
int						check_can_do_solution(t_data *d);
int						cal_expose1(t_data *d);
int						cal_expose2(t_data *d);

int						fill_expose_left_1(t_data *d);
int						fill_expose_left_2(t_data *d);
int						fill_expose_left_3(t_data *d);
void					fill_expose_left_4(t_data *d);
void					fill_expose_left_5(t_data *d);

int						error(char const *str);
int						init_start(t_data **d, int ac, char **av);
int						open_log(t_data *d);
t_data					*get_data(void);
void					w_log(char const *str);

float					ft_atof(char const *str);
void					ft_putfloat(float nbr);
float					ft_sqrt(float nbr);

#endif
