#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jmoiroux <jmoiroux@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/04/02 15:15:21 by jmoiroux          #+#    #+#              #
#    Updated: 2015/04/02 15:15:25 by jmoiroux         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

# change with $(GCC) or $(CLANG) depend system
CC = $(CLANG)

# change your includes
HEADER = includes/projet.h

# change binary name
NAME = computor

# add sources files
SRCS =	src/main.c \
		src/basic_functions.c \
		src/init_start.c \
		src/count_expos.c \
		src/init_tab_expose.c \
		src/fill_expose_left.c \
		src/fill_expose_left_2.c \
		src/free_exit.c \
		src/print_expose_left.c \
		src/check_can_do_solution.c \
		src/calculate.c \
		src/cal_expose1.c \
		src/cal_expose2.c \
		src/add_libft/ft_atof.c \
		src/add_libft/ft_putfloat.c \
		src/add_libft/ft_sqrt.c


# Don'y modify following
GCC = gcc
CLANG = clang
CFLAGS = -Wall -Werror -Wextra -pedantic -o3
LIB = libft/libft.a
OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(LIB):
	make -C libft

%.o: %.c $(HEADER)
	@$(CC) $(CFLAGS) -c $< -o $@

$(NAME): $(LIB) $(OBJS) $(HEADER)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME) $(LIB)

clean:
	make clean -C libft
	rm -rf $(OBJS)

fclean: clean
	make fclean -C libft
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
